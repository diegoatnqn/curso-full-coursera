$(function () {
    $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $('.carousel').carousel({
    interval: 2000
        });
        $('#aviso').on('show.bs.modal', function (e) {
    console.log("modal abriendose");
            $('.btn-reserva').removeClass('btn-primary btn-sm');
            $('.btn-reserva').addClass('btn-danger btn-sm ');
            $('.btn-reserva').prop('disabled', true);
        })
        $('#aviso').on('shown.bs.modal', function (e) {
    console.log("modal abierto");
        })
        $('#aviso').on('hide.bs.modal', function (e) {
    console.log("Modal cerrandose");
            $('.btn-reserva').removeClass('btn-danger');
            $('.btn-reserva').addClass('btn-primary btn-sm');
            $('.btn-reserva').prop('disabled', false);
        })
        $('#aviso').on('hidden.bs.modal', function (e) {
    console.log("modal cerrado");
        })
    });